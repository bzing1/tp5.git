/**
 * 关闭打开窗口，并重新加载页面
 */
function closeWindow(){
    var index = parent.layer.getFrameIndex(window.name);
    window.parent.location.reload(index);
}

/**
 * 显示隐藏下级
 * @param obj this对象
 * @param id
 * @param deep 深度，0为顶级，1为0的下级，以此类推
 */
function onShow(obj,id,deep){
    const data = $(".pid_"+id).find(".operate a").attr("data");
    (deep === 0 && data === '1') ? $(".topid_"+id).show() : $(".pid_"+id).show();
    $(obj).parents("tr").find(".operate span").append('<a onClick="onHide(this,'+id+','+deep+')" data="1"><span><i class="Hui-iconfont">&#xe6a1; </i> </span></a>');
    $(obj).remove();
}

/**
 * 隐藏下级
 * @param obj this对象
 * @param id
 * @param deep 深度，0为顶级，1为0的下级，以此类推
 */
function onHide(obj,id,deep){
    deep === 0 ? $(".topid_"+id).hide() : $(".pid_"+id).hide();
    $(obj).parents("tr").find(".operate span").append('<a onClick="onShow(this,'+id+','+deep+')" data="0"><span><i class="Hui-iconfont">&#xe600; </i> </span></a>');
    $(obj).remove();
}

/**
 * 二级列表引用显示下级
 * @param obj
 * @param id
 */
function subShow(obj,id){
    $(".top_"+id).show();
    $(obj).parents("tr").find(".operate").prepend('<a onClick="subHide(this,'+id+')"><span><i class="Hui-iconfont">&#xe6a1; </i> </span></a>');
    $(obj).remove();
}

/**
 * 二级列表引用隐藏下级
 * @param obj
 * @param id
 */
function subHide(obj,id){
    $(".top_"+id).hide();
    $(obj).parents("tr").find(".operate").prepend('<a onClick="subShow(this,'+id+')"><span><i class="Hui-iconfont">&#xe600; </i> </span></a>');
    $(obj).remove();
}

/**
 * 修改状态
 * @param obj
 * @param route
 * @param id
 * @param status
 * @param msg
 */
function status(obj,route,id,status,msg) {
    const status1 = status === 1 ? 2 : 1;
    const msg1 = status === 1 ? '禁用' : '启用';
    const font = status === 1 ? '&#xe605;' : '&#xe605;';
    const label = status === 1 ? 'success' : 'defaunt';
    layer.confirm('确认要'+ msg +'吗？',function(){
        $.ajax({
            type: 'post',
            url: route,
            data: {id:id, status:status},
            success:function(data){
                if(data.code === -2) {
                    layer.msg(data.msg, { icon:5, time:3000 }, function () {
                        location.href = "<?php echo url('login/login')?>";
                    });
                } else if(data.code !== 0){
                    layer.msg(data.msg,{ icon:5, time:2000 });
                } else {
                    $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="status(this,\''+route+'\','+id+','+status1+',\''+msg1+'\')" href="javascript:;" title="'+msg1+'"><i class="Hui-iconfont">'+font+'</i></a>');
                    $(obj).parents("tr").find(".td-status").html('<span class="label label-'+label+' radius">已'+msg+'</span>');
                    $(obj).remove();
                    layer.msg('已'+ msg +'！',{icon: 1,time:1000});
                }
            }
        });
    });
}

/**
 * 打开窗口权限验证
 * @param msg
 * @param rote
 * @param w
 * @param h
 */
function show(msg, rote, w='', h='') {
    $.ajax({
        type: 'post',
        url: '/admin/auth/jumpCheck',
        data: {rule: rote},
        success:function(data){
            if(data.code === -2) {
                layer.msg(data.msg, { icon:5, time:3000 }, function () {
                    location.href = "<?php echo url('login/login')?>";
                });
            } else if(data.code !== 0){
                layer.msg(data.msg,{ icon:5, time:2000 });
            }  else {
                layer_show(msg, rote, w, h);
            }
        }
    });
}

/**
 * 自动上传
 * @param url  上传地址
 * @param fid  上传文件id
 * @param img_id  image id,并且作为文件目录文件夹
 * @returns {boolean}
 */
function ajaxFileUpload(url, fid, img_id) {
    $.ajaxFileUpload({
        url: url, //用于文件上传的服务器端请求地址
        type: 'post',
        secureuri: false, //是否需要安全协议，一般设置为false
        fileElementId: fid, //文件上传域的ID
        dataType: 'json', //返回值类型 一般设置为json
        async : true,   //是否是异步
        data: {file_name: img_id},  //携带参数
        success: function (data) {  //服务器成功响应处理函数
            if (data.code !== 0) {
                layer.msg(data.msg,{ icon:5, time:2000 });
            } else {
                layer.msg('上传成功！',{ icon:1, time:1000 });
                $('#'+img_id).attr('src',data.data.path);
            }
        },
        error: function (data, status, e) {  //服务器响应失败处理函数
            layer.msg('上传失败，服务器未响应！',{ icon:5, time:2000 });
        }
    });
    return false;
}