<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/8/27
 * Time: 17:42
 */
namespace app\wechat\controller;

class Login extends Base {
    protected $token;

    public function __construct(){
        $this->needLogin = false;
        parent::__construct();
    }

//    /**
//     * 已授权登录
//     * @return string|\think\response\Json
//     */
//    public function authorizeLogin(){
//        try {
//            if ( empty($this->params['code']) ) {
//                throw new \Exception('参数错误', -1);
//            }
//            self::_getOpenidSessionKey();  // 获取openid和seesionkey
//
//            self::_saveLoginInfo();  // 保存登录信息
//
//        } catch(\Exception $e) {
//            return _error($e->getCode(),$e->getMessage());
//        }
//        return _success(['token'=>$this->token]);
//    }

    /**
     * 登录
     * @return string|\think\response\Json
     */
    public function login(){
        try {
            if ( empty($this->params['code'])
                || empty($this->params['encryptedData'])
                || empty($this->params['rawData'])
                || empty($this->params['signature'])
                || empty($this->params['iv']) ) {
                throw new \Exception('参数错误', -1);
            }
            self::_getOpenidSessionKey();  // 获取openid和seesionkey

            self::_checkSession();  // 验签

            self::_getDecryptInfo();  // 解密用户信息

            self::_saveLoginInfo();  // 保存登录信息

        } catch(\Exception $e) {
            return _error($e->getCode(),$e->getMessage());
        }
        return _success(['token'=>$this->token]);
    }
}
