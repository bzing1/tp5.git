<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/8/28
 * Time: 14:01
 */
namespace app\wechat\controller;

 class User extends Base{
     /**
      * 获取用户信息
      * @return string|\think\response\Json
      */
     public function getUserInfo(){
         try {
             if ( empty($this->params['encryptedData'])
                 || empty($this->params['rawData'])
                 || empty($this->params['signature'])
                 || empty($this->params['iv']) ) {
                 throw new \Exception('参数错误', -1);
             }
             $info = $this->wx_user->find($this->id);
             if ( empty($info) ) {
                 throw new \Exception('该用户不存在', -1);
             }

             $this->openid = $info['openid'];
             $this->session_key = $info['session_key'];

             self::_checkSession();  // 签名

             self::_getDecryptInfo();  // 解密用户信息

             self::_saveUserInfo();  // 保存用户信息

         } catch(\Exception $e) {
             return _error($e->getCode(),$e->getMessage());
         }
         return _success();
     }
 }