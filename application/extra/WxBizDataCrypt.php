<?php
/**
 * 对微信小程序用户加密数据的解密示例代码.
 * @copyright Copyright (c) 1998-2014 Tencent Inc.
 */

class WXBizDataCrypt {
    /**
     * @var string appid
     */
    private $appid;

    /**
     * @var string session key
     */
	private $sessionKey;

    /**
     *  -41001: encodingAesKey 非法
     *  -41003: aes 解密失败
     *  -41004: 解密后得到的buffer非法
     *  -41005: base64加密失败
     *  -41016: base64解密失败
     */
    protected static $OK = 0;
    protected static $IllegalAesKey = -41001;
    protected static $IllegalIv = -41002;
    protected static $IllegalBuffer = -41003;
    protected static $DecodeBase64Error = -41004;

	/**
	 * 构造函数
	 * @param $sessionKey string 用户在小程序登录后获取的会话密钥
	 * @param $appid string 小程序的appid
	 */
	public function __construct( $appid, $sessionKey ){
		$this->sessionKey = $sessionKey;
		$this->appid = $appid;
	}

    /**
     * 检验数据的真实性，并且获取解密后的明文.
     * @param $encryptedData string 加密的用户数据
     * @param $iv string 与用户数据一同返回的初始向量
     * @param $data string 解密后的原文
     * @throws Exception
     */
	public function decryptData( $encryptedData, $iv, &$data ){
		if (strlen($this->sessionKey) != 24) {
            throw new \Exception('encodingAesKey 非法', self::$IllegalAesKey);
		}
		$aesKey=base64_decode($this->sessionKey);

		if (strlen($iv) != 24) {
            throw new \Exception('iv 非法', self::$IllegalIv);
		}
		$aesIV=base64_decode($iv);

		$aesCipher=base64_decode($encryptedData);

		$result=openssl_decrypt( $aesCipher, "AES-128-CBC", $aesKey, 1, $aesIV);

		$dataObj=json_decode( $result );
		if( $dataObj  == NULL ) {
            throw new \Exception('aes 解密失败', self::$IllegalBuffer);
		}
		if( $dataObj->watermark->appid != $this->appid ) {
            throw new \Exception('aes 解密失败', self::$IllegalBuffer);
		}
		$data = json_decode($result, true);
	}
}

