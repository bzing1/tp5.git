<?php
/**
 * 自定义常量类
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/8/2
 * Time: 14:35
 */

class Param{

    //角色
    const GROUP_ADMIN = 1;   // 超级管理员

    //状态
    const STATUS_DEFAULT  = 0;   // 默认
    const STATUS_ON       = 1;   // 启用
    const STATUS_OFF      = 2;   // 禁用
    const STATUS_DEL      = 3;   // 删除

    //分类类型
    const LOG_CATEGORY   = 2;  // 系统日志
    const LOG_TYPE_LOGIN = 3;  // 日志登录
    const LOG_TYPE_ADD   = 4;  // 日志新增
    const LOG_TYPE_EDIT  = 5;  // 日志编辑
    const LOG_TYPE_DEL   = 6;  // 日志删除


}