<?php
/**
 * Created by PhpStorm.
 * User: zhangbing
 * Date: 2017/5/20
 * Time: 16:16
 */
namespace app\admin\validate;
use think\Validate;
class Rule extends Validate{
    protected $rule = [
        'rule_name' => 'require|unique:auth_rule',
        'auth_rule' => 'unique:auth_rule',
    ];
    //定义错误信息
    protected $message = [
        'rule_name.require'  => '权限名称不能为空',
        'rule_name.unique'   => '权限名称不能重复',
        'auth_rule.unique'   => '验证规则不能重复'
    ];
    //验证场景
    protected $scene = [
        'ruleAdd'  =>  ['rule_name','auth_rule'],
        'ruleSave' =>  ['rule_name','auth_rule'],
    ];
}