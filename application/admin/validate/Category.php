<?php
/**
 * Created by PhpStorm.
 * User: zhangbing
 * Date: 2017/5/20
 * Time: 16:16
 */
namespace app\admin\validate;
use think\Validate;
class Category extends Validate{
    protected $rule = [
        'category_name' => 'require',
    ];
    //定义错误信息
    protected $message = [
        'category_name.require'  => '分类名称不能为空',
    ];
    //验证场景
    protected $scene = [
        'add'  =>  ['category_name'],
        'save' =>  ['category_name'],
    ];
}