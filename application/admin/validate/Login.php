<?php
/**
 * Created by PhpStorm.
 * User: zhangbing
 * Date: 2017/5/20
 * Time: 16:16
 */
namespace app\admin\validate;
use think\Validate;
class Login extends Validate{
    //定义规则
    protected $rule = [
        'username' => 'require',
        'password' => 'require',
        'captcha'  => 'require|captcha',
    ];
    //定义错误信息
    protected $message = [
        'username.require' => '用户名不能为空',
        'password.require' => '密码不能为空',
        'captcha.require'  => '验证码不能为空',
        'captcha.captcha'  => '验证码不正确',
    ];
    //验证场景
    protected $scene = [
        'login' => ['username','password','captcha'],
    ];
}