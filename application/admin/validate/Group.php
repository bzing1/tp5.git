<?php
/**
 * Created by PhpStorm.
 * User: zhangbing
 * Date: 2017/5/20
 * Time: 16:16
 */
namespace app\admin\validate;
use think\Validate;
class Group extends Validate{
    protected $rule = [
        'group_name' => 'require|unique:auth_group',
        'auth_ids' => 'require',
    ];
    protected $message = [
        'group_name.require' => '用户组名称不能为空',
        'group_name.unique'  => '用户组名称必须唯一',
        'auth_ids.require'   => '用户组权限不能为空',
    ];
    //定义场景
    protected $scene = [
        'groupAdd' => ['group_name','auth_ids'],
        'groupSave' => ['group_name','auth_ids'],
    ];
}