<?php
/**
 * Created by PhpStorm.
 * User: zhangbing
 * Date: 2017/5/20
 * Time: 16:16
 */
namespace app\admin\validate;
use think\Validate;
class Admin extends Validate{
   //定义规则
    protected $rule = [
        'username' => 'require|length:2,20|unique:admin',
        'password' => 'require|length:6,18',
    ];
    //定义错误信息
    protected $message = [
        'username.require'  => '用户名不能为空',
        'username.length'   => '用户名长度为2-20位',
        'username.unique'   => '用户名已存在',
        'password.require'  => '密码不能为空',
        'password.length'   => '密码长度为6-18位',
    ];
    //验证场景
    protected $scene = [
        'adminAdd' => ['username','password'],
    ];
}