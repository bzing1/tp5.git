<?php
/**
 * 系统管理类
 * Created by PhpStorm.
 * User: zhangbing
 * Date: 2018/7/20
 * Time: 15:45
 */
namespace app\admin\controller;

class System extends Base{
    protected function _initialize(){
        if ($this->request->action() == 'logShow') {
            $this->needAuth  = false;  // 关闭权限验证
        }
        parent::_initialize();
    }
    /**
     * 基础配置
     * @return array|\think\response\View
     */
    public function base(){
        $config = get_config();
        if(IS_POST){
            $result = compare($this->params, $config);  //参数对比, 拼接变动的参数，无改动直接返回
            if(!$result) return _success();

            foreach($this->params as $k=>$v){
                if($v != $config[$k]){
                    $this->setValue($this->config, ['key'=>$k], 'value', $v);
                    $config[$k] = $v;
                }
            }
            $this->logs[] = ['修改基础配置', \Param::LOG_TYPE_EDIT, "修改基础配置{$result}"];
            logs($this->logs);  // 添加操作日志
            return _success();
        }
        return view('base',['config'=>$config]);
    }

    /**
     * 系统日志
     * search 参数 $this->params:表单参数 input精确查找 时间查找 select状态查找
     * @return \think\response\View
     */
    public function log(){
        $data = [];
        if(isset($this->params['page']) || IS_POST){
            $search = search($this->params,['b.username','a.title','a.uip'],['a.create_time'],['a.type']);
            $where  = $search['where'];
            $list   = $this->admin_log
                ->alias('a')
                ->field('a.*,b.username,c.category_name')
                ->where($where)
                ->join([['admin b','a.uid = b.id','inner'],['category c','a.type = c.id','inner']])
                ->order('a.id desc')
                ->paginate(10, false, ['query'=>$this->params, 'var_page'=>'page']);
            $data = ['list'=>$list, 'params'=>$search['params']];
        }
        $data['category'] = $this->getList($this->category, ['status'=>\Param::STATUS_ON, 'pid'=>\Param::LOG_CATEGORY]);
        return view('log',$data);
    }

    /**
     * 日志详情
     * @return \think\response\View
     */
    public function logShow(){
        $join = [['admin b','a.uid = b.id','inner']];
        $where = "a.id = '{$this->params['id']}'";
        $field = 'a.*,b.username';
        $info = $this->joinOne($this->admin_log, $join, $where, $field);
        return view('logShow',['info'=>$info]);
    }
}